from pymongo import MongoClient
import sys, time
from datetime import date
class DAO:
    db = None
    def __init__(self):
        try:
            client = MongoClient('localhost:27017')
            self.db = client.quotelang
        except Exception as erro:
            print('erro ao conectar-se'+erro)

    def adicionar(self, dic):
        try:
            self.db.frases.insert(dic)
            print('dados de '+ dic['autor'] + ' em '+dic['idioma']+ ' inseridos com sucesso!')
        except Exception as erro:
            print(erro)
            self.registraLog('erroInsercao', erro)

            sys.exit(1)

    def getData(self):
        r = []
        try:
            for d in self.db.frases.find():
                r.append(d)
            return r

        except Exception as erro:
            self.registraLog('erro',erro)

    def checarAutor(self, nome):
        dt = []
        for i in self.db.frases.find({'nome_url':nome}):
            dt.append(i)
        return dt


    def registraLog(self, tipo, log):
        print('registrando log... ')
        dt = self.EncontraLog(tipo)
        if dt:
            dt['logs'].append(log)
            self.db.log.update({'_id': dt['_id']},
                               {"$set": {'data': str(date.fromtimestamp(time.time())), 'tipo': tipo, 'logs': dt['logs']}})
        else:
            id = 0
            dt = self.getLog()
            if dt == True:
                id = int(dt[-1]['_id'])
            self.db.log.insert({'_id': id + 1, 'data': str(date.fromtimestamp(time.time())), 'tipo': tipo, 'logs': [log]})


    def EncontraLog(self, tipo):
        r = None
        for d in self.db.log.find({'tipo':tipo}):
            r = d
        return r

    def getLog(self):
        r = []
        try:
           if self.db.log.find():
               for d in self.db.log.find():
                   r.append(d)
           else:
               return []

           # print(r)
           # return r
        except Exception as erro:
            self.registraLog('erro',erro)
