from dev.util.funcoes import formatar, filtrar
from dev.model.DAO import DAO
from dev.crawler.op import scrap
import sys

class Frances():
    d = None
    dados = []
    url = 'https://fr.wikiquote.org/wiki/'
    def __init__(self):
        self.d = DAO()
    def Fetch_Fr(self, soup, nome_url):
        dados = self.d.getData()
        id  =  dados[-1]['_id'] +1 if len(dados)>0 else 1
        frase = [filtrar(i.text) for i in soup.findAll('div', {'class': 'citation'})]
        fonte = [filtrar(i.text) for i in soup.findAll('div', {'class': 'ref'})]
        autor = soup.find('h1',{'id':'firstHeading'}).text
        aux = [{'frases':frase,'fontes':fonte}]
        dados = formatar('fr', autor, aux,  nome_url, id)
        self.d.adicionar(dados)


    def montarUrl(self, autor, idioma):

        if len(self.d.checarAutor(autor))>0:
            status = 'todos os dados de ' + autor + ' em '+idioma+'  já estão atualizados!'
            self.d.registraLog('status', status)
            print("Registro de dados atualizado registrado!")
            return
        else:
            print('****** montando url **** ')
            endereco = self.url + autor
            print(endereco)
            self.Fetch_Fr(scrap(endereco), autor)


