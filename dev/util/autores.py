

autores_geral = ('Steve_Jobs','Bill_Gates','Donald_Trump','Mark_Zuckerberg')
autores_fr  = (
                  'Anacharsis',
                  'Diogène_de_Sinope',
                  'Socrate',
                  'Protagoras',
                  'Platon',
                  'Jean_Henri_van_Swinden',
                  'Baruch_Spinoza',
                  'José_Ortega_y_Gasset',
                  'Confucius',
                  'Érasme'
              )