#Quick Intro

This is a simple project using Python as a web crawler in order to fetch information from the Wikiquotes in French and register it in mongoDB.

To run it, install the requirements as listed in the requirements.txt file and make sure you have an instance of mongodb running!