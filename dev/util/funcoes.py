from unicodedata import normalize

def formatar(idioma,autor, dados,nome_url, id = 1):
    return {
        '_id': id,
        'idioma': idioma,
        'autor':autor,
        'nome_url':nome_url,
        'dados': dados
        }
def filtrar(data):
    return normalize('NFKC', data)