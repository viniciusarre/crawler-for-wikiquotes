from dev.fetch.fr import Frances
from dev.util.autores import autores_fr, autores_geral
#import random


class Main():
    lang = ('fr', 'it', 'pt', 'es', 'en')
    #ind = random.randint(0,lang.index(lang[-1]))
    ind = 0

    def selecionaIdioma(self):
        if self.lang[self.ind] == 'fr':
            f = Frances()
            print('***** percorrendo autores em frances *******')
            for i in range(len(autores_fr)):
                f.montarUrl(autores_fr[i],self.lang[self.ind])
            print('***** percorrendo autores gerais em francês ****** ')
            for j in range(len(autores_geral)):
                f.montarUrl(autores_geral[j],self.lang[self.ind])



